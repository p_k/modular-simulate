import React, { Component } from 'react'
import { PropTypes } from 'prop-types'

import { View, StyleSheet, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

class BannerContainer extends Component {
  static PropTypes = {
    contentHeader: PropTypes.string,
    contentTitle: PropTypes.string,
    options: PropTypes.object.isRequired,
    styleProps: PropTypes.object.isRequired
  }
  static defaultProps = {
    contentHeader: 'Header',
    contentTitle: 'title',
    options: {},
    styleProps: {
    }
  }
  constructor(props) {
    super(props)
  }

  extactStyle(key) {
    const { styleProps } = this.props
    return styleProps[key] || {}
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: 'transparent',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center',
        ...this.extactStyle('container')
      },
      messageBox: {
        justifyContent: 'center',
        ...this.extactStyle('messageBox')
      },
      linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        ...this.extactStyle('linearGradient')
      },
      contentHeader: {
        color: 'white',
        justifyContent: 'center',
        fontSize: 40,
        ...this.extactStyle('contentHeader')
      },
      contentTitle: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        opacity: 0.8,
        ...this.extactStyle('contentTitle')
      }
    })
    return (
      <LinearGradient colors={
      [
        'grey',
        'black',
        'black',
        'black'
      ]}
        style={styles.linearGradient}>
        <View style={styles.container}>
          <View style={styles.messageBox}>
            <Text style={styles.contentHeader}>{this.props.contentHeader.toUpperCase()}</Text>
            <Text style={styles.contentTitle}>{this.props.contentTitle}</Text>
          </View>
        </View>
      </LinearGradient>
    )
  }
}


export default BannerContainer
