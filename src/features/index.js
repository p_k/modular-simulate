// export feature
export { Feed, FeedContainer } from './feed'

// export { BannerContainer } from './Echo'
export { BannerContainer } from './banner'

// export reducer
export { default as reducers } from './reducers'
